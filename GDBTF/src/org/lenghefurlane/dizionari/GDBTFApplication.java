package org.lenghefurlane.dizionari;

import android.app.Application;
import android.content.Context;

import java.io.InputStream;

public class GDBTFApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        GDBTFApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return GDBTFApplication.context;
    }

}
