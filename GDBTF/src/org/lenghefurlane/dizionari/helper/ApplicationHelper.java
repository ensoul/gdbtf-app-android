package org.lenghefurlane.dizionari.helper;

import org.lenghefurlane.dizionari.objects.Definizione;
import org.lenghefurlane.dizionari.objects.Ricerca;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

public class ApplicationHelper {

    public enum ValueType {
        Integer,
        Boolean,
        String
    }
	
	public static List<Definizione> DEFINIZIONI;
	public static Ricerca RICERCA_SELECTED;
	public static String LEMMA;

	public static Context CURRENTCONTEXT;
	
	public static void setSharedPreferencesValue(String key, Object value)
	{
		boolean success = false;
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(CURRENTCONTEXT);
		
		SharedPreferences.Editor editor = preferences.edit();
		
		if(value instanceof Integer)
		{
			editor.putInt(key, (Integer) value);
			success = true;
		}
		
		if(value instanceof String)
		{
			editor.putString(key, (String) value);
			success = true;
		}

		if(success)
			editor.commit();
	}
	
	public static Object getSharedPreferencesValue(String key, ValueType type)
	{
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(CURRENTCONTEXT);
		
		if(type == ValueType.Integer)
			return preferences.getInt(key, 0);
		
		if(type == ValueType.String)
			return preferences.getString(key, "");
		
		return null;
	}
	
	public static void clearSharedPreferences(boolean clearall)
	{
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(CURRENTCONTEXT);
		
		SharedPreferences.Editor editor = preferences.edit();
		
		if(clearall)
			editor.clear();
		else
		{
			Map<String,?> prefs = preferences.getAll();
			
			for(Map.Entry<String,?> entry : prefs.entrySet()){
				
				if( !entry.getKey().startsWith("ZOOM") )
					editor.remove(entry.getKey());
			}
	 
		}
		
		editor.commit();
	}

	public static void removeSharedPreferences(String key)
	{
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(CURRENTCONTEXT);

		if(preferences.contains(key))
		{

			SharedPreferences.Editor editor = preferences.edit();

			editor.remove(key);

			editor.commit();
		}
	}

	public static Object getSharedPreferencesValue(String key)
	{
		if(CURRENTCONTEXT == null)
			return null;

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(CURRENTCONTEXT);

		Object result = null;

		try
		{
			result = preferences.getString(key, null);
			return result;
		}
		catch(Exception e){}

		try
		{
			result = preferences.getInt(key, 0);
			return result;
		}
		catch(Exception e){}

		try
		{
			result = preferences.getLong(key, new Long(-1));
			return result;
		}
		catch(Exception e){}

		try
		{
			result = preferences.getBoolean(key, new Boolean(false));
			return result;
		}
		catch(Exception e){}

		try
		{
			result = preferences.getFloat(key, new Float(-1));
			return result;
		}
		catch(Exception e){}

		return null;
	}

	public static int dpToPx(int dp)
	{
		return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
	}

	public static int pxToDp(int px)
	{
		return (int) (px / Resources.getSystem().getDisplayMetrics().density);
	}
}
