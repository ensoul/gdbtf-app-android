package org.lenghefurlane.dizionari.helper;

import org.lenghefurlane.dizionari.setup.ApplicationSetup;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class ConnectivityHelper {

	public static boolean isOnline() {
	    
		ConnectivityManager cm =  (ConnectivityManager)ApplicationHelper.CURRENTCONTEXT.getSystemService(Context.CONNECTIVITY_SERVICE);
	    
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}
	
	public static boolean isOnlineWithToast()
	{
		if(ApplicationSetup.DEBUG)
			return true;
		
		ConnectivityManager cm =  (ConnectivityManager)ApplicationHelper.CURRENTCONTEXT.getSystemService(Context.CONNECTIVITY_SERVICE);
	    
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	   
		
		Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Connessione internet assente", Toast.LENGTH_LONG);
		toast.show();
		
		return false;
	}
	
	public static void showOfflineToast()
	{
		Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Connessione internet assente", Toast.LENGTH_SHORT);
		toast.show();
	}
	
	public static void showCachedOfflineToast()
	{
		Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Ricerca in modalità offline", Toast.LENGTH_SHORT);
		toast.show();
	}
	
}
