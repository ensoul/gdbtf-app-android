package org.lenghefurlane.dizionari.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.lenghefurlane.dizionari.GDBTFApplication;
import org.lenghefurlane.dizionari.objects.MessageObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.URI;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class HttpTaskHelper extends AsyncTask<Void, Void, Void> {

    Handler _mHandler;

    int _tiporichiesta = -1;

    String _output = "";
    String _txtUrl;
    ProgressDialog _progress;

    public HttpTaskHelper(Context context, Handler h, String txtUrl, int tiporichiesta) {

        _progress = new ProgressDialog(context);
        _progress.setCancelable(false);
        _progress.setMessage("Attendere...");

        _txtUrl = txtUrl;
        _tiporichiesta = tiporichiesta;
        _mHandler = h;

    }

    @Override
    public void onPreExecute() {
        _progress.show();
    }

    @Override
    protected Void doInBackground(Void... params) {

        MessageObject mo = null;
        URI uri;
        try {

			uri = new URI(_txtUrl);

			HttpParams httpParameters = new BasicHttpParams();

			int timeoutConnection = 60*1000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);

			int timeoutSocket = 60*1000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

			HttpClient client = new DefaultHttpClient(httpParameters);
			HttpGet request = new HttpGet(uri);

            HttpResponse response = client.execute(request);
            _output = HttpHelper.request(response);

            CacheFifo.putCache(_txtUrl, _output);

            mo = new MessageObject(_output, _tiporichiesta);

        } catch (Exception e) {
            mo = new MessageObject(e.getMessage(), _output, _tiporichiesta);
            e.printStackTrace();
        }

        Message msg = _mHandler.obtainMessage();
        msg.obj = mo;
        _mHandler.sendMessage(msg);

        return null;
    }

    @Override
    public void onPostExecute(Void unused) {
        _progress.dismiss();
    }

    public String getOutput() {
        return _output;
    }

}


