package org.lenghefurlane.dizionari.helper;

import org.lenghefurlane.dizionari.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public class ViewHelper {

	public static View getSottoDefinizioneView(Context context)
	{
		View viewSottoDefinizione = LayoutInflater.from(context).inflate(R.layout.sottodefinizione, null);
		
		return viewSottoDefinizione;
	}
	
}
