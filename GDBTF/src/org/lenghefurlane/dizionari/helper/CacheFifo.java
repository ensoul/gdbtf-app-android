package org.lenghefurlane.dizionari.helper;

import android.text.TextUtils;
import android.util.Log;

import org.lenghefurlane.dizionari.setup.ApplicationSetup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CacheFifo
{

    // Cyclic cache
    //    key1,key2,key3
    //
    //   if(key.count > 500){
    //        keys.remove(key1)
    //        //-> key2,key3
    //    }

//    import Foundation
//
//    let words = "apple binary cat delta echo".components(separatedBy: " ")
//    print(words)
    // let array = Array(results) // la fin

    public static int LIMIT = ApplicationSetup.DEBUG ? 5 : 500;
    public static String KEYS = "KEYS";
    public static String CACHE_FIFO = "CACHE_FIFO";

    public static List<String> getKeys(){

        String keys = (String) ApplicationHelper.getSharedPreferencesValue(KEYS);
        if(TextUtils.isEmpty(keys))
            return new ArrayList<String>();

        String[] keyArray = keys.split(",",-1);

        return Arrays.asList(keyArray);
    }

    public static String keysListToString(List<String> keysList){

        String result = "";
        for(String s:keysList)
            result += s + ",";

        return result;
    }

    public static void putCache(String key, String value){

        setupCache();

        List<String> keysList = getKeys();

        if(   !keysList.contains(key)
           && keysList.size() > LIMIT){

            String firstKey = keysList.get(0);

            List<String> keyListCloned = new ArrayList<String>();
            for(int i=1; i<keysList.size(); i++)
                keyListCloned.add(keysList.get(i));

            keysList = keyListCloned;

            ApplicationHelper.removeSharedPreferences(firstKey);

        }

        ApplicationHelper.setSharedPreferencesValue(KEYS, keysListToString(keysList) + key);
        ApplicationHelper.setSharedPreferencesValue(key,value);

        if(ApplicationSetup.DEBUG){
            Log.d("CacheFifo", String.format("size: %s", keysList.size()));
            Log.d("CacheFifo", String.format("contains: %s", keysListToString(getKeys())) );
        }
    }

    private static void setupCache(){

        String CACHEFIFO = (String) ApplicationHelper.getSharedPreferencesValue(CACHE_FIFO);
        if(CACHEFIFO == null){
            ApplicationHelper.clearSharedPreferences(true);
            ApplicationHelper.setSharedPreferencesValue(CACHE_FIFO,"1");
        }
    }
}
