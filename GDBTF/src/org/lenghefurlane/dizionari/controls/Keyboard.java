package org.lenghefurlane.dizionari.controls;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import org.lenghefurlane.dizionari.R;

/**
 * Created by ale on 08/10/16.
 */

public class Keyboard
{
    //    à è ì ò ù
    //    â ê î ô û

    private Activity _a;

    protected LinearLayout _llKeyboard = null;

    public Keyboard(Activity a, final EditText etRicerca){

        _a = a;

        _llKeyboard = (LinearLayout) _a.findViewById(R.id.llKeyboard);

        ImageButton ibKeyboard = (ImageButton) _a.findViewById(R.id.ibKeyboard);
        ibKeyboard.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(_llKeyboard.getVisibility() == View.VISIBLE)
                    hideKeyboard();
                else
                    showKeyboard();
            }
        });

        final Button bnKey1 = (Button) _a.findViewById(R.id.bnKey1);
        bnKey1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey1);
            }
        });

        final Button bnKey1a = (Button) _a.findViewById(R.id.bnKey1a);
        bnKey1a.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey1a);
            }
        });

        final Button bnKey1b = (Button) _a.findViewById(R.id.bnKey1b);
        bnKey1b.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey1b);
            }
        });

        final Button bnKey2 = (Button) _a.findViewById(R.id.bnKey2);
        bnKey2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey2);
            }
        });

        final Button bnKey2a = (Button) _a.findViewById(R.id.bnKey2a);
        bnKey2a.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey2a);
            }
        });

        final Button bnKey3 = (Button) _a.findViewById(R.id.bnKey3);
        bnKey3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey3);
            }
        });

        final Button bnKey3a = (Button) _a.findViewById(R.id.bnKey3a);
        bnKey3a.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey3a);
            }
        });

        final Button bnKey4 = (Button) _a.findViewById(R.id.bnKey4);
        bnKey4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey4);
            }
        });

        final Button bnKey4a = (Button) _a.findViewById(R.id.bnKey4a);
        bnKey4a.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey4a);
            }
        });

        final Button bnKey5 = (Button) _a.findViewById(R.id.bnKey5);
        bnKey5.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey5);
            }
        });

        final Button bnKey5a = (Button) _a.findViewById(R.id.bnKey5a);
        bnKey5a.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addWord(etRicerca, bnKey5a);
            }
        });

        hideKeyboard();
    }


    public void showKeyboard(){

        _llKeyboard.setVisibility(View.VISIBLE);
    }

    public void hideKeyboard(){

        _llKeyboard.setVisibility(View.GONE);
    }

    protected void addWord(EditText to, Button from)
    {
        String text = to.getText().toString();
        text += from.getText().toString();
        to.setText(text);
    }
}
