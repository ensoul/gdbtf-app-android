package org.lenghefurlane.dizionari.controls;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;

public class CustomWebView extends WebView {

	public CustomWebView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
    public CustomWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }
    
    public CustomWebView(Context context, AttributeSet attrs)
    {
    	super(context,attrs);
    }

	public final OnTouchListener mOnTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent rawEvent) {
			return false;
		}
	};

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		
		return false;
	}
}
