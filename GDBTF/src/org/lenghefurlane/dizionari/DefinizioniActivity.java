package org.lenghefurlane.dizionari;

import org.lenghefurlane.dizionari.adapters.DefinizioneAdapter;
import org.lenghefurlane.dizionari.helper.ApplicationHelper;
import org.lenghefurlane.dizionari.objects.Definizione;
import org.lenghefurlane.dizionari.objects.Ricerca;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.ListView;
import android.widget.TextView;

public class DefinizioniActivity extends GDBTFActivity{

	private DefinizioneAdapter _definizioneAdapter;
	private ListView _definizione_lv;
	private List<Definizione> _definizioni;
	private String _lemma = "";
	
	private TextView _titledefinizione_tv;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.form_definizione);
    
        ApplicationHelper.CURRENTCONTEXT = this.getApplicationContext();

        _titledefinizione_tv = (TextView) findViewById(R.id.definizione_item_title);

		if(getIntent().getExtras() != null)
		{
			String lingua = getIntent().getExtras().getString("lingua");
			String lemma = getIntent().getExtras().getString("lemma");

			_ricercaSelected = new Ricerca();
			_ricercaSelected.setUid(lemma);
			_ricercaSelected.setTipo(0);

			InviaRichiestaLemma();
		}
		else
		{
			setupDefinizione();
		}
	}

	private void setupDefinizione()
	{
		_definizioni = ApplicationHelper.DEFINIZIONI;
		_lemma = ApplicationHelper.LEMMA;

		if(_lemma == null){
			_titledefinizione_tv.setText("-");
		}
		else {

			try {
				_titledefinizione_tv.setText(Html.fromHtml(_lemma));
			} catch (Exception e) {
				_titledefinizione_tv.setText(_lemma);
			}
		}

		_definizione_lv = (ListView) findViewById(R.id.definizione_lv);

		_definizioneAdapter = new DefinizioneAdapter(this,R.layout.definizione_item, _definizioni);
		_definizione_lv.setAdapter(_definizioneAdapter);

	}

	@Override
	protected void ShowDefinizioniActivity(List<Definizione> definizioni,String lemma)
	{
		ApplicationHelper.DEFINIZIONI = definizioni;
		ApplicationHelper.LEMMA = lemma;

		if(this instanceof DefinizioniActivity){

			setupDefinizione();
		}
		else{

			Intent i = new Intent();
			i.setClass(this, DefinizioniActivity.class);
			startActivity(i);
		}
	}
}
