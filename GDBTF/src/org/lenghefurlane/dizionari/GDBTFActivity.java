package org.lenghefurlane.dizionari;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.Vibrator;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lenghefurlane.dizionari.helper.ApplicationHelper;
import org.lenghefurlane.dizionari.helper.ConnectivityHelper;
import org.lenghefurlane.dizionari.helper.HttpTaskHelper;
import org.lenghefurlane.dizionari.objects.Definizione;
import org.lenghefurlane.dizionari.objects.Frase;
import org.lenghefurlane.dizionari.objects.MessageObject;
import org.lenghefurlane.dizionari.objects.Ricerca;
import org.lenghefurlane.dizionari.setup.ApplicationSetup;
import org.lenghefurlane.dizionari.utils.CustomTextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ale on 30/08/16.
 */
public class GDBTFActivity extends AppCompatActivity
{
    protected android.support.v7.app.ActionBar _actionBar;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        _actionBar = getSupportActionBar();

        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.action_bar, null);
        _actionBar.setCustomView(mCustomView);
        _actionBar.setDisplayShowCustomEnabled(true);
    }

    protected Ricerca _ricercaSelected;

    public static int RICHIESTALEMMA = 1;
    public static int RICERCA = 2;

    protected void getRequest(String txtUrl, int tiporichiesta){

        if(ApplicationSetup.DEBUG)
            Log.v("REQUEST",txtUrl);

        if(!ConnectivityHelper.isOnline())
        {
            String response = (String) ApplicationHelper.getSharedPreferencesValue(txtUrl,ApplicationHelper.ValueType.String);

            if(response.isEmpty())
            {
                ConnectivityHelper.showOfflineToast();

                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(400);
            }
            else
                ConnectivityHelper.showCachedOfflineToast();

            if( !response.isEmpty() )
                manageOutput(response, tiporichiesta);
        }
        else
        {
            HttpTaskHelper task = new HttpTaskHelper(GDBTFActivity.this,handler,txtUrl,tiporichiesta);
            task.execute();
        }
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            MessageObject mo = (MessageObject) msg.obj;

            manageOutput(mo.getError(), mo.getOutput(), mo.getTipoRichiesta());
        }
    };

    protected void manageOutput(String req, int tiporichiesta)
    {
        manageOutput("", req, tiporichiesta);
    }

    protected void setupNoData(){}

    protected boolean hasError(JSONObject jsonObject) throws Exception{

        if(jsonObject.has("error"))
            return true;

        return false;
    }

    protected void manageOutput(String error, String req, int tiporichiesta)
    {
        if(!TextUtils.isEmpty(error))
        {
            Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Riprovare tra qualche istante", Toast.LENGTH_LONG);
            toast.show();

            return;
        }

        if(TextUtils.isEmpty(req))
        {
            Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Nessun risultato", Toast.LENGTH_LONG);
            toast.show();
            setupNoData();

            return;
        }

        if(tiporichiesta == RICHIESTALEMMA)
        {
            try {

                JSONObject jObject = new JSONObject(req);
                int type = jObject.getInt("type");

                _ricercaSelected.setTipo(type);

                parseLEMMA(req);

            } catch (JSONException e) {

                Toast.makeText(this, "Si è verificato un'errore", Toast.LENGTH_SHORT).show();

                e.printStackTrace();
            }
        }
    }

    protected void ShowDefinizioniActivity(List<Definizione> definizioni,String lemma)
    {
        ApplicationHelper.DEFINIZIONI = definizioni;
        ApplicationHelper.LEMMA = lemma;

        Intent i = new Intent();
        i.setClass(this, DefinizioniActivity.class);
        startActivity(i);
    }

    protected void InviaRichiestaLemma()
    {
        if(TextUtils.isEmpty(_ricercaSelected.getUid()))
            return;

        String query = ApplicationSetup.getEngineUrl() + ApplicationSetup.LEMMA + _ricercaSelected.getUid();

        getRequest(query,RICHIESTALEMMA);
    }

    protected String getStringFromJArray(JSONObject jObject, String key) throws JSONException {

        String result = "";

        JSONArray ja = jObject.getJSONArray(key);

        for (int i = 0; i < ja.length(); i++)
        {
            String cgs = ja.getString(i);
            if(i == ja.length() - 1)
                result += cgs;
            else
                result += cgs + ", ";
        }

        return result;
    }

    protected void parseLEMMA(String jString) throws JSONException {

        String title = "";
        List<Definizione> definizioni = new ArrayList<Definizione>();

        JSONObject jObject = new JSONObject(jString);

        String lemma = jObject.getString("lemma");
        String apex = jObject.getString("apex");
        String cgs = getStringFromJArray(jObject,"cgs");
        int type = jObject.getInt("type");

        if(!TextUtils.isEmpty(apex) && !apex.equals("null"))
            apex = String.format("<sup><small>%s</small></sup>", apex);
        else apex = "";

        title += String.format("(%s %s) %s",lemma, apex, cgs);

        JSONObject type_props = jObject.getJSONObject("type_props");

        if(type == Ricerca.CONFISSO && type_props.has("etimology")){

            String etimology = type_props.getString("etimology");
            title += String.format(" [%s]",etimology);
        }

        if(type == Ricerca.LEMMA_MONOREMATICO){

            if(type_props.has("uas"))
            {
                JSONArray uas = type_props.getJSONArray("uas");

                boolean has_roman = uas.length() > 1;

                for (int i = 0; i < uas.length(); i++)
                {
                    JSONObject ua = uas.getJSONObject(i);

                    if (has_roman)
                    {
                        Definizione defRoman = new Definizione();
                        defRoman.setRomanIndex(i);
                        definizioni.add(defRoman);
                    }

                    parseBAS(ua.getJSONArray("bas"), definizioni, type);
                }
            }
        }
        else
        {
            JSONArray bas = type_props.getJSONArray("bas");
            parseBAS(bas, definizioni, type);
        }

        ShowDefinizioniActivity(definizioni, title);

    }

    protected void parseBAS(JSONArray bas, List<Definizione> definizioni, int lemma_type) throws JSONException {

        for (int i = 0; i < bas.length(); i++)
        {
            JSONObject ba = bas.getJSONObject(i);
            JSONObject ds = null;

            if (lemma_type != Ricerca.CONFISSO)
                ds = ba.getJSONObject("main_ds");
            else
                ds = ba.getJSONObject("ds");

            String definizione_result = "";
            String degree = ba.getString("degree");
            String subdegree = ba.getString("subdegree");

            definizione_result += degree + subdegree + " ";

            if (lemma_type != Ricerca.CONFISSO)
            {
                String cgs = CustomTextUtils.removeBrackets(ba.getString("cgs"));
                if(!TextUtils.isEmpty(cgs))
                    definizione_result += CustomTextUtils.joinString(cgs);
            }
            else
            {
                String css = CustomTextUtils.removeBrackets(ba.getString("css"));
                if(!TextUtils.isEmpty(css))
                    definizione_result += "[TS] " + CustomTextUtils.joinString(css);
            }

            Definizione definizione = parseDS(ds, lemma_type);
            definizione.setText1(definizione_result + " " + definizione.getText1());
            definizione.setDegree(degree);
            definizione.setSubDegree(subdegree);

            if (lemma_type != Ricerca.CONFISSO)
            {
                JSONArray weak_forms = ds.getJSONArray("weak_forms");

                for (int k = 0; k < weak_forms.length(); k++) // forme deboli primo livello
                {
                    JSONObject weak_form = weak_forms.getJSONObject(k);

                    Definizione def_weak = parseDS(weak_form, lemma_type);
                    definizione.getSottoDefinizioneList().add(def_weak);

                    JSONArray weak_forms2 = weak_form.getJSONArray("weak_forms");

                    for (int j = 0; j < weak_forms2.length(); j++) // forme deboli secondo livello
                    {
                        JSONObject weak_form2 = weak_forms2.getJSONObject(j);
                        Definizione def_weak2 = parseDS(weak_form2, lemma_type);
                        definizione.getSottoDefinizioneList().add(def_weak2);
                    }
                }
            }

            definizioni.add(definizione);
        }
    }

    protected Definizione parseDS(JSONObject ds, int lemma_type) throws JSONException{

        Definizione definizione = new Definizione();

        if(lemma_type != Ricerca.CONFISSO){

            String mus = CustomTextUtils.removeBrackets(CustomTextUtils.removeQuotations(ds.getString("mus")));

            if(!TextUtils.isEmpty(mus))
                definizione.appendText1(String.format("[%s]",mus));

            String css = CustomTextUtils.removeBrackets(ds.getString("css"));
            String ccs = CustomTextUtils.removeBrackets(ds.getString("ccs"));

            definizione.appendText1( CustomTextUtils.joinString(css) + CustomTextUtils.joinString(ccs) );
        }

        String definition = CustomTextUtils.removeBrackets(ds.getString("definition"));
        String translation = CustomTextUtils.removeBrackets(ds.getString("translations"));

        definizione.setDefinizione(getStringFromJArray(ds,"translations"));
        //definizione.setDefinizione(CustomTextUtils.removeQuotations(translation));
        if(!definition.isEmpty())
            definizione.appendText2("("+definition+")" + " ");

        JSONArray sentences = ds.getJSONArray("sentences");

        for(int i=0; i<sentences.length(); i++){

            String frase_ita = sentences.getJSONArray(i).getString(0);
            String frase_fur = "\n"+sentences.getJSONArray(i).getString(1);

            Frase frase = new Frase();
            frase.setFraseIta(frase_ita);
            frase.setFraseFur(frase_fur);

            definizione.getFrasi().add(frase);

        }

        return definizione;
    }

}
