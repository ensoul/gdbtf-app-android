package org.lenghefurlane.dizionari;

import org.lenghefurlane.dizionari.helper.ApplicationHelper;
import org.lenghefurlane.dizionari.setup.ApplicationSetup;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashScreen extends Activity {

	protected boolean _active = true;
	protected int _splashTime = 3000; 
	
	private Thread _splashTread = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    setContentView(R.layout.form_splashscreen);
	 
	    ApplicationHelper.CURRENTCONTEXT = this.getApplicationContext();
	  
	    if(_splashTread == null)
	    {
		    _splashTread = new Thread() {
		        @Override
		        public void run() {
		            
		        	try {
		        		
		        		if(!ApplicationSetup.DEBUG)
		        		{
		        			
			                int waited = 0;
			                while(_active && (waited < _splashTime)) {
			                    sleep(100);
			                    if(_active) {
			                        waited += 100;
			                    }
			                }
			                
		        		}
		                
		            } catch(InterruptedException e) {
		            
		            } finally {
		            	
		                NextActivity();
		                //stop();
		            }
		        }
		    };
	    
	    }
	    
	    if(!_splashTread.isAlive())
	    	_splashTread.start();
	}

	private void NextActivity()
	{
		finish();

		Intent intent=new Intent(this , org.lenghefurlane.dizionari.AdvertiseSplashScreen.class);
		startActivity(intent);
	}

}
