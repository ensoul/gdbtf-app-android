package org.lenghefurlane.dizionari.utils;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;

public class TextStyle {

	public static SpannableStringBuilder getStyledText(Context context, int style, String text)
	{
		SpannableStringBuilder ssb = new SpannableStringBuilder(text);
	    TextAppearanceSpan tas = new TextAppearanceSpan(context, style);
	    ssb.setSpan(tas, 0, ssb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
	    return ssb;
	}
	
	public static SpannableStringBuilder getStyledText(Context context, int style, Spanned text)
	{
		SpannableStringBuilder ssb = new SpannableStringBuilder(text);
	    TextAppearanceSpan tas = new TextAppearanceSpan(context, style);
	    ssb.setSpan(tas, 0, ssb.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
	    return ssb;
	}
	
	public static SpannableStringBuilder getStyledText(Context context, int style1, int style2, String text1, String text2)
	{
		String text = text1 + text2;
		
		SpannableStringBuilder ssb = new SpannableStringBuilder(text);
	    TextAppearanceSpan tas1 = new TextAppearanceSpan(context, style1);
	    TextAppearanceSpan tas2 = new TextAppearanceSpan(context, style2);
	    ssb.setSpan(tas1, 0, text1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	    ssb.setSpan(tas2, text1.length(), text1.length() + text2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
	    return ssb;
	}

	public static SpannableStringBuilder getStyledText(Context context, int style1, int style2, int style3,
													   String text1, String text2, String text3)
	{
		String text = text1 + text2 + text3;

		SpannableStringBuilder ssb = new SpannableStringBuilder(text);

		TextAppearanceSpan tas1 = new TextAppearanceSpan(context, style1);
		TextAppearanceSpan tas2 = new TextAppearanceSpan(context, style2);
		TextAppearanceSpan tas3 = new TextAppearanceSpan(context, style3);

		ssb.setSpan(tas1, 0, text1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		ssb.setSpan(tas2, text1.length(), text1.length() + text2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		ssb.setSpan(tas3, text1.length() + text2.length(), text1.length() + text2.length() + text3.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		return ssb;
	}

	public static SpannableStringBuilder getStyledText(Context context, int style1, int style2, String text1, String text2, int stylepattern, String pattern) {

		text2  = " " + text2;
		
		String allText = text1 + text2;
		
		Elements elements = Jsoup.parse(allText).select("span");
		
		String parola = "";
		
		for(Element element:elements)
		{
			pattern = element.toString();
			parola = element.childNode(0).toString();
			break;
		}
		
		if(!parola.isEmpty())
		{
			text1 = text1.replace(pattern, parola);
			text2 = text2.replace(pattern, parola);
			
			allText =  text1 + text2;
			pattern = parola;
		}

		SpannableStringBuilder ssb = null;
		
	    List<IndexBookmark> indexs1 = new ArrayList<IndexBookmark>();
	    List<IndexBookmark> indexs2 = new ArrayList<IndexBookmark>();
	    
	    // find all occurrences forward
	    for (int i = -1; (i = text1.indexOf(pattern, i + 1)) != -1; ) {
	      
	    	IndexBookmark idx = new IndexBookmark( i, i+pattern.length() );
	    	indexs1.add(idx);
	    } 
	    
	    for (int i = -1; (i = text2.indexOf(pattern, i + 1)) != -1; ) {
		      
	    	IndexBookmark idx = new IndexBookmark( i, i+pattern.length() );
	    	indexs2.add(idx);
	    } 
	    
	    ssb = new SpannableStringBuilder(allText);
	    
	    ssb = getSpannedString(context, style1, stylepattern, ssb, text1, indexs1, -1);
	    ssb = getSpannedString(context, style2, stylepattern, ssb, text2, indexs2, text1.length());
	    
		return ssb;
		
	}
	
	private static SpannableStringBuilder getSpannedString(Context context, int style, int stylepattern, SpannableStringBuilder ssb, String text, List<IndexBookmark> indexs, int offset)
	{
		if(offset == -1)
			offset = 0;
		
		int k = 0;
	    for(int i=0;i<text.length(); )
	    {
	    	if(k<indexs.size())
	    	{
	    		IndexBookmark ib = indexs.get(k);
	    		
	    		if(i < ib.getIdxStart())
	    		{
	    			ssb.setSpan(new TextAppearanceSpan(context, style), offset+i, offset+ib.getIdxStart(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	    			ssb.setSpan(new TextAppearanceSpan(context, stylepattern), offset+ib.getIdxStart(), offset+ib.getIdxEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	    			
	    			i=ib.getIdxEnd();
	    			k++;
	    		}
	    	}
	    	else
	    	{
	    		ssb.setSpan(new TextAppearanceSpan(context, style), offset+i, offset+text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	    		i=text.length();
	    	}
	    }
		
		return ssb;
	}
	
	public static class IndexBookmark
	{
		private int _idx_start;
		private int _idx_end;
		
		public IndexBookmark(int start,int end)
		{
			_idx_start = start;
			_idx_end = end;
		}
		
		public int getIdxStart()
		{
			return _idx_start;
		}
		
		public int getIdxEnd()
		{
			return _idx_end;
		}
	}
	
	public static String getMirrored(List<Character> list)
	{
		StringBuilder sb = new StringBuilder();
		
		for(int i = list.size()-1; i>=0; i--)
		{
			sb.append(list.get(i));
		}
		
		return sb.toString();
	}
	
}
