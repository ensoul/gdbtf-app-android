package org.lenghefurlane.dizionari.utils;

import android.text.Html;
import android.text.Spanned;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ale on 21/10/16.
 */

public class CustomTextUtils
{
    public static String joinString(String value){

        if(android.text.TextUtils.isEmpty(value))
            return "";

        return " " + removeQuotations(value);
    }

    public static String adjustWord(String word)
    {
        word = word.replaceAll("\\s+", " ");
        word = word.trim();
        word = word.replaceAll(" ", "%20");

        return word;
    }

    public static String removeBrackets(String value){

        value = value.replace("[","");
        value = value.replace("]","");

        return value;
    }

    public static String removeQuotations(String value){

        value = value.replace("\"","");

        return value;
    }

    public static Spanned getApex(String value)
    {

        Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(value);

        if (m.find())
        {

            String apex = m.group();
            String text = value.replace(m.group(), "");
            apex = String.format(" <sup><small>%s</small></sup> ", apex.replace("(", "").replace(")", ""));

            return Html.fromHtml(text + apex);
        }

        return null;
    }
}
