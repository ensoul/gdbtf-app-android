package org.lenghefurlane.dizionari.adapters;

import org.lenghefurlane.dizionari.R;
import org.lenghefurlane.dizionari.objects.KeyValueObject;
import org.lenghefurlane.dizionari.utils.CustomTextUtils;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LemmaAdapter extends ArrayAdapter<KeyValueObject> {

		private int resourceId = 0;
		private LayoutInflater inflater;
		private Context context;

		public LemmaAdapter(Context context, int resourceId,
				List<KeyValueObject> objects) {
			super(context, resourceId, objects);

			this.resourceId = resourceId;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.context = context;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			View view = null;
			
			if (convertView == null)
				view = LayoutInflater.from(context).inflate(R.layout.lemma_item, null);
			else
				view = convertView;

			KeyValueObject item = getItem(position);

			LinearLayout ll_main = (LinearLayout)view.findViewById(R.id.lemma_item_ll);

			if(position % 2 == 0)
				ll_main.setBackgroundColor(Color.parseColor("#e0dad1"));
			else
				ll_main.setBackgroundColor(Color.parseColor("#ede9e4"));

			TextView tv = (TextView)view.findViewById(R.id.lemma_item_text);

			Spanned apex = CustomTextUtils.getApex(item.getValue());

			if(apex != null)
				tv.setText(apex);
			else
				tv.setText(item.getValue());

			Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
	        view.startAnimation(animation);
			
			return view;
		}
}
