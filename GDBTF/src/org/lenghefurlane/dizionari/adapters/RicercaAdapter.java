package org.lenghefurlane.dizionari.adapters;

import org.lenghefurlane.dizionari.R;
import org.lenghefurlane.dizionari.objects.Ricerca;
import org.lenghefurlane.dizionari.utils.CustomTextUtils;
import org.lenghefurlane.dizionari.utils.TextStyle;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class RicercaAdapter extends ArrayAdapter<Ricerca> {

	private int resourceId = 0;
	private LayoutInflater inflater;
	private Context context;

	public RicercaAdapter(Context context, int resourceId,	List<Ricerca> objects) {
		
		super(context, resourceId, objects);

		this.resourceId = resourceId;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = null;
		
		view = LayoutInflater.from(context).inflate(R.layout.ricerca_item, null);

		Ricerca item = getItem(position);
		
		TextView tv_text = (TextView)view.findViewById(R.id.ricerca_item_text1);
		TextView tv_page = (TextView)view.findViewById(R.id.ricerca_item_text2);

		Button bn_next = (Button)view.findViewById(R.id.ricerca_item_next);
		
		LinearLayout ll_text = (LinearLayout)view.findViewById(R.id.ricerca_item_tv_ll);
		LinearLayout ll_main = (LinearLayout)view.findViewById(R.id.ricerca_item_ll);

		tv_page.setVisibility(View.GONE);
		
//		if(item.getPage().isEmpty())
//		{
//			tv_page.setVisibility(View.GONE);
//		}
//		else
//		{
//			tv_page.setText(item.getPage());
//			tv_page.setVisibility(View.VISIBLE);
//		}
		
		if(position % 2 == 0)
			ll_main.setBackgroundColor(Color.parseColor("#e0dad1"));
		else
			ll_main.setBackgroundColor(Color.parseColor("#ede9e4"));
		
		if(item.getPaging())
		{
			ll_text.setVisibility(View.GONE);
			bn_next.setVisibility(View.VISIBLE);
		}
		else
		{
			ll_text.setVisibility(View.VISIBLE);
			bn_next.setVisibility(View.GONE);

			ImageView ricerca_iv = (ImageView)view.findViewById(R.id.ricerca_iv);

			tv_text.setTypeface(null, Typeface.NORMAL);

			if (item.getTipo() == Ricerca.LEMMA_MONOREMATICO)
			{
				ricerca_iv.setVisibility(View.VISIBLE);
				ricerca_iv.setImageResource(R.drawable.definizione_poli);
				tv_text.setTypeface(null, Typeface.BOLD);
			} else
			{
				ricerca_iv.setVisibility(View.GONE);
			}

			if(item.getLemmi().size() > 0 ){
				tv_text.setText( String.format("%s (%s)", item.getText(), item.getLemmi().size() ));
			}
			else
			{
				Spanned apex = CustomTextUtils.getApex(item.getText().toString());

				if(apex != null)
					tv_text.setText(apex);
				else
				{
					if(item.getTipo() == Ricerca.LEMMA_POLIREMATICO)
						tv_text.setText(Html.fromHtml("<i>"+item.getText()+"</i>"));
					else
						tv_text.setText(item.getText());
				}

			}

		}
		 
        Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
        view.startAnimation(animation);

        return view;
	}

}
