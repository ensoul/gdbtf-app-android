package org.lenghefurlane.dizionari.adapters;

import org.lenghefurlane.dizionari.DefinizioniActivity;
import org.lenghefurlane.dizionari.R;
import org.lenghefurlane.dizionari.helper.ViewHelper;
import org.lenghefurlane.dizionari.objects.Definizione;
import org.lenghefurlane.dizionari.objects.Frase;
import org.lenghefurlane.dizionari.utils.RomanNumbers;
import org.lenghefurlane.dizionari.utils.TextStyle;

import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Html;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DefinizioneAdapter extends ArrayAdapter<Definizione> {

	private Context _context;

	public DefinizioneAdapter(Context context, int resourceId,List<Definizione> objects) {
		super(context, resourceId, objects);

		_context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = LayoutInflater.from(_context).inflate(R.layout.definizione_item, null);
		
		final Definizione item = getItem(position);

		LinearLayout ll_main = (LinearLayout)view.findViewById(R.id.definizione_item_ll);
		LinearLayout ll_definizione = (LinearLayout)view.findViewById(R.id.definizioneLayout);
		LinearLayout ll_roman = (LinearLayout)view.findViewById(R.id.romanSeparatorLayout);
		
		if(item.isRoman())
		{
			TextView romanSeparatorTextView = (TextView)view.findViewById(R.id.romanSeparatorTextView);
			
			ll_roman.setVisibility(View.VISIBLE);
			ll_definizione.setVisibility(View.GONE);
			
			String romanNumber = RomanNumbers.binaryToRoman( item.getRomanIndex()+1 ).toString();
			romanNumber = "."+romanNumber;
			
			romanSeparatorTextView.setText(romanNumber);
		}
		else
		{
			ll_roman.setVisibility(View.GONE);
			ll_definizione.setVisibility(View.VISIBLE);

			if(position % 2 == 0)
				ll_main.setBackgroundColor(Color.parseColor("#e0dad1"));
			else
				ll_main.setBackgroundColor(Color.parseColor("#ede9e4"));
			
			TextView tv1 = (TextView)view.findViewById(R.id.definizione_item_text_1);
			TextView tv2 = (TextView)view.findViewById(R.id.definizione_item_text_2);
			LinearLayout ll_frasi = (LinearLayout)view.findViewById(R.id.definizione_item_frasi);
			LinearLayout ll_sottofrasi = (LinearLayout)view.findViewById(R.id.definizione_item_sottofrasi);

			String txt = item.getText1();

			try
			{
				if (txt.contains("["))
				{
					String txt1a = item.getDegree() + item.getSubDegree();
					String txt1b = txt.substring(txt.indexOf(txt1a)+txt1a.length(), txt.indexOf('['));
					String txt1c = txt.substring(txt.indexOf('['));

					tv1.setText(TextStyle.getStyledText(
							_context, R.style.FontBold, R.style.FontItalic, R.style.FontNormal,
							txt1a, txt1b, txt1c));
				} else
				{
					String txt1a = item.getDegree() + item.getSubDegree();
					String txt1b = txt.substring(txt.indexOf(txt1a)+txt1a.length());

					tv1.setText(TextStyle.getStyledText(
							_context, R.style.FontBold, R.style.FontItalic,
							txt1a, txt1b));
				}
			}
			catch(Exception e){
				tv1.setText(txt);
			}

			if(item.getText2().contains("a href='#'"))
				createButton(item.getText2(),ll_main);
			else
				tv2.setText(  TextStyle.getStyledText(
						_context, R.style.FontItalic, R.style.FontBoldBlue, item.getText2(),item.getDefinizione()  ) );

			for(Frase frase:item.getFrasi())
			{
				LinearLayout frase_ll = (LinearLayout)LayoutInflater.from(_context).inflate(R.layout.frase, null).findViewById(R.id.frase_ll);
				
				TextView tv = (TextView)frase_ll.findViewById(R.id.frase_textview);
				
				tv.setText( TextStyle.getStyledText(_context, R.style.FontNormal, R.style.FontItalicLightBlue, frase.getFraseIta(), frase.getFraseFur(), R.style.FontBoldBlue, "span") );
				ll_frasi.addView(frase_ll);
			}
			
			if(item.getFrasi().size() == 0)
				ll_frasi.setVisibility(View.GONE);

			if(item.getSottoDefinizioneList().size() == 0)
			{
				View viewSottoDefinizione = ViewHelper.getSottoDefinizioneView(_context);
				LinearLayout ll_sottofrasi_sottodef = (LinearLayout)viewSottoDefinizione.findViewById(R.id.definizione_item_sottofrasi);
				ll_sottofrasi_sottodef.setVisibility(View.GONE);
			}
			
			int counter = 0;
			for(Definizione sottoDefinizione:item.getSottoDefinizioneList())
			{
				View viewSottoDefinizione = ViewHelper.getSottoDefinizioneView(_context);
				
				LinearLayout ll_sottofrasi_sottodef = (LinearLayout)viewSottoDefinizione.findViewById(R.id.definizione_item_sottofrasi);
				LinearLayout ll_separator = (LinearLayout)viewSottoDefinizione.findViewById(R.id.separator_ll);
				TextView tv2b = (TextView)viewSottoDefinizione.findViewById(R.id.definizione_item_text_2b);
				
				if(sottoDefinizione == null)
				{
					tv2b.setVisibility(View.GONE);
					ll_sottofrasi_sottodef.setVisibility(View.GONE);
				}
				else
				{
					tv2b.setVisibility(View.VISIBLE);
					ll_sottofrasi_sottodef.setVisibility(View.VISIBLE);

					if(sottoDefinizione.getText2().contains("a href='#'"))
					{
						//coloc.","regjon. (<a href='#' class='riflemma' id='2-1_38898'>
						// <span class='black'>peperoncino</span></a>)

						createButton(sottoDefinizione.getText2(), ll_sottofrasi_sottodef);

						try
						{
							String head = sottoDefinizione.getText2();
							int idx = head.indexOf("(<a");

							if( idx == -1)
								idx = head.indexOf("<a");

							if(idx > -1)
								head = head.substring(0, idx);

							head = head.replace("\"", "");
							tv2b.setText(TextStyle.getStyledText(_context, R.style.FontBoldBlue, head));
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}
					else
					{
						String sottodefinizione = sottoDefinizione.getText1() + sottoDefinizione.getText2() + " ";
					
						tv2b.setText(  TextStyle.getStyledText(
								_context, R.style.FontNormal, R.style.FontBoldBlue, sottodefinizione, sottoDefinizione.getDefinizione()  ) );
					}

					for(Frase frase:sottoDefinizione.getFrasi())
					{
						
						LinearLayout frase_ll = (LinearLayout)LayoutInflater.from(_context).inflate(R.layout.frase, null).findViewById(R.id.frase_ll);
						
						TextView tv = (TextView)frase_ll.findViewById(R.id.frase_textview);
						tv.setText( TextStyle.getStyledText(_context, R.style.FontNormal, R.style.FontItalicLightBlue, frase.getFraseIta(), frase.getFraseFur(), R.style.FontBoldBlue, "span") );
						
						ll_sottofrasi_sottodef.addView(frase_ll);
					}
					
				}
				
				ll_sottofrasi.addView(viewSottoDefinizione);
				
				if(counter < item.getSottoDefinizioneList().size()-1)
				{
					ll_separator.setVisibility(View.VISIBLE);
					
//					if(position % 2 == 0)
//						ll_separator.setBackgroundColor(Color.rgb( 241, 237, 241));
//					else
//						ll_separator.setBackgroundColor(Color.rgb( 225, 222, 225));
				}
				else
					ll_separator.setVisibility(View.GONE);
				
				counter++;
			}
		}

		Animation animation = AnimationUtils.loadAnimation(_context, android.R.anim.fade_in);
        view.startAnimation(animation);
		
		return view;
	}
	
	public void createButton(String html, LinearLayout ll)
	{
		//<a href='#' class='riflemma' id='2-1_26123'><span class='black'>nottolino</span></a>
		//(<a href='#' class='riflemma' id='1-1_40230'><span class='black'>gena<sup>1</sup></span></a>)
		
		Document doc = Jsoup.parse(html);
        Elements element = doc.getAllElements();
        String word = "";
		String apex = "";
        
        String id = element.attr("id");
        if(doc.getElementsByTag("span").size() == 1)
		{
			if (doc.getElementsByTag("span").get(0).childNodes().size() >= 1)
			{
				word = doc.getElementsByTag("span").get(0).childNode(0).toString().replace("~", "");
				if (word.length() > 0 && word.charAt(0) == ' ')
					word = word.substring(1, word.length());

				if (doc.getElementsByTag("span").get(0).childNodes().size() >= 2){
					apex = doc.getElementsByTag("span").get(0).childNode(1).toString().replace("~", "");
					if (apex.length() > 0 && apex.charAt(0) == ' ')
						apex = apex.substring(1, apex.length());

					word += apex;
				}

			}
		}
        
        if(id.contains("-"))
        {
        	String[] splitted = id.split("-");
        	if(splitted.length == 1)
        		id = splitted[0];
        	else if(splitted.length == 2)
        		id = splitted[1];
        }
		
		Button bnMore = new Button(_context);
		bnMore.setLayoutParams( new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		Drawable img = getContext().getResources().getDrawable( R.drawable.book_open );
		bnMore.setCompoundDrawablesWithIntrinsicBounds(null, null, img, null);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		{
			bnMore.setAllCaps(false);
		}
		bnMore.setText(Html.fromHtml(word));
		bnMore.setGravity(Gravity.CENTER);
		
		ll.addView(bnMore);
		
		bnMore.setOnClickListener( new onClickListener(id,word) );
	}
	
	public class onClickListener implements View.OnClickListener
	{
		String _word = "";
		String _id = "";
		
		public onClickListener(String id, String word)
		{
			_word = word;
			_id = id;
		}

		@Override
		public void onClick(View v) {
			
			Intent intent=new Intent(_context, DefinizioniActivity.class);
	        intent.putExtra("lingua", "ITA");
	        intent.putExtra("parola", _word);
			intent.putExtra("lemma", _id);
	        _context.startActivity(intent);
			
		}
	
	}
	
}
