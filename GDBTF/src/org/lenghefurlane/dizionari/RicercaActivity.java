package org.lenghefurlane.dizionari;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lenghefurlane.dizionari.adapters.RicercaAdapter;
import org.lenghefurlane.dizionari.controls.Keyboard;
import org.lenghefurlane.dizionari.helper.ApplicationHelper;
import org.lenghefurlane.dizionari.objects.KeyValueObject;
import org.lenghefurlane.dizionari.objects.Ricerca;
import org.lenghefurlane.dizionari.setup.ApplicationSetup;
import org.lenghefurlane.dizionari.utils.CustomTextUtils;

import java.util.ArrayList;
import java.util.List;

public class RicercaActivity extends GDBTFActivity {

	private Dialog _cancellaDialog;

	private int _currentLanguage = -1;
	private int _currentForm = 1;

	private static int FORM_RICERCA = 1;

	private int _pagineTotali = -1;
    private int _paginaCorrente = -1;
    private String _parolaCorrente ="";

    private List<Ricerca> _ricerche;
    private RicercaAdapter _ricercaAdapter;
	private ListView _ricerca_lv;
	private Keyboard _keyboard;

	private EditText _etRicerca;
	private Button _ibRicerca;
	private boolean _canExit = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {

    	super.onCreate(savedInstanceState);

//        if(android.os.Build.VERSION.SDK_INT < 11)
//            requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.form_gdbtf);

        ApplicationHelper.CURRENTCONTEXT = this.getApplicationContext();

        _ricerca_lv = (ListView) findViewById(R.id.ricerca_lv);

        _ricercaAdapter = new RicercaAdapter(this,R.layout.ricerca_item, new ArrayList<Ricerca>());
		_ricerca_lv.setAdapter(_ricercaAdapter);
		_ricerca_lv.setOnItemClickListener(new OnCustomItemClickListener());
		_ricerca_lv.setDivider(null);



		_ibRicerca =  (Button)findViewById(R.id.ibSearch);
		_ibRicerca.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				_etRicerca.getText().clear();
				_ricercaAdapter.clear();
				_ricercaAdapter.notifyDataSetChanged();
			}
		});

		_etRicerca = (EditText)findViewById(R.id.etSearch);

		_etRicerca.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				if(event.getAction() == MotionEvent.ACTION_UP)
					((EditText)v).selectAll();
				return false;
			}
		});

		if(ApplicationSetup.DEBUG)
			_etRicerca.setText("cane");

		if(getIntent().getExtras() != null)
		{
			String lingua = getIntent().getExtras().getString("lingua");
			String parola = getIntent().getExtras().getString("parola");

			 _etRicerca.setText(parola);
			 searchItaliano();
			 _canExit=false;
		}

		if(savedInstanceState != null && !savedInstanceState.isEmpty()) {

			_parolaCorrente = savedInstanceState.getString("PAROLACORRENTE");
			_currentLanguage = savedInstanceState.getInt("CURRENTLANGUAGE");

			if(_currentLanguage == Ricerca.FUR)
			{
				searchFurlan();
			}
			else if(_currentLanguage == Ricerca.ITA)
			{
				searchItaliano();
			}

		}

		_keyboard = new Keyboard(this, _etRicerca);
    }

    @Override
	protected void onSaveInstanceState(Bundle bundle) {

		bundle.putString("PAROLACORRENTE", _parolaCorrente);
		bundle.putInt("CURRENTLANGUAGE", _currentLanguage);

		super.onSaveInstanceState(bundle);
	}

    public void ButtonNextPageClick(View view) {

		incrementaPagina();
    }

    public void ButtonPrevPageClick(View view) {


		decrementaPagina();
    }

    public void ButtonItalianoClick(View view) {

    	searchItaliano();

    }

    public void searchItaliano()
    {

    	if(_ricerche != null)
    		_ricerche.clear();

    	_ricercaAdapter.clear();

    	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(_etRicerca.getWindowToken(), 0);

		_etRicerca.clearFocus();

        _parolaCorrente = _etRicerca.getText().toString();

        if(_parolaCorrente.isEmpty())
        {
        	Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Campo ricerca vuoto", Toast.LENGTH_LONG);
			toast.show();

			return;
        }

        _paginaCorrente = 1;

        _currentLanguage = Ricerca.ITA;

        InviaRichiestaRicerca(String.valueOf(_paginaCorrente));

    }

    public void searchFurlan()
    {
    	if(_ricerche != null)
    		_ricerche.clear();

    	_ricercaAdapter.clear();

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(_etRicerca.getWindowToken(), 0);

		_etRicerca.clearFocus();

        _parolaCorrente = _etRicerca.getText().toString();

        if(_parolaCorrente.isEmpty())
        {
        	Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Campo ricerca vuoto", Toast.LENGTH_LONG);
			toast.show();

			return;
        }

        _paginaCorrente = 1;

        _currentLanguage = Ricerca.FUR;

        InviaRichiestaRicerca(String.valueOf(_paginaCorrente));

    }

    public void ButtonFriulanoClick(View view) {

    	searchFurlan();

    }

	@Override
    protected void manageOutput(String error, String req, int tiporichiesta)
    {
		if(!TextUtils.isEmpty(error))
		{
			Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Riprovare tra qualche istante", Toast.LENGTH_LONG);
			toast.show();

			return;
		}

		if(req.isEmpty())
		{
			Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Nessun risultato", Toast.LENGTH_LONG);
			toast.show();
			setupNoData();

			return;
		}

		super.manageOutput(error, req, tiporichiesta);

		if(tiporichiesta == RICERCA)
    	{
    		try {

    			if(_ricercaAdapter.getCount() > 1)
    				_ricercaAdapter.remove(_ricercaAdapter.getItem(_ricercaAdapter.getCount()-1));

    			if(_currentLanguage == Ricerca.ITA)
    			{
    				parseSearch_ITA(req);
    			}
    			else if(_currentLanguage == Ricerca.FUR)
    			{
    				parseSearch_FUR(req);
    			}

    		} catch (Exception e) {

    			e.printStackTrace();
    		}

    	}

    	RefreshRicercaListView();

    }

    private void InviaRichiestaRicerca(final String pagina)
    {
        String query = "";

        _parolaCorrente = CustomTextUtils.adjustWord(_parolaCorrente);

		if(_currentLanguage == Ricerca.ITA)
		{
			query = ApplicationSetup.getEngineUrl()+"search?st="+_parolaCorrente+"&page="+pagina;
		}
		else if(_currentLanguage == Ricerca.FUR)
		{
			query = ApplicationSetup.getEngineUrl()+"searchfr?st="+_parolaCorrente+"&page="+pagina;
		}

    	getRequest(query,RICERCA);

    }

	private void ShowLemmiActivity()
	{
		if(_ricercaSelected.getLemmi().size() == 0)
			return;

		ApplicationHelper.RICERCA_SELECTED = _ricercaSelected;

		Intent i = new Intent();
		i.setClass(this, LemmiActivity.class);
		startActivity(i);
	}

	private void setupSuggestion(JSONObject jObject) throws Exception
	{
		boolean is_suggestion = false;

		if(jObject.has("is_suggestion"))
			is_suggestion = jObject.getInt("is_suggestion") == 1;

		if(is_suggestion){
			Ricerca ricerca = new Ricerca();

			ricerca.setText("Peraule no cjatade, cirivistu forsit ...?");
			ricerca.setUid("");
			ricerca.setTipo(0);
			ricerca.setLanguage(_currentLanguage);

			if(_ricerche == null)
				_ricerche = new ArrayList<Ricerca>();

			_ricerche.add(ricerca);
			_ricercaAdapter.add(ricerca);
		}
	}

	@Override
	protected void setupNoData()
	{
		Ricerca ricerca = new Ricerca();

		ricerca.setText("Peraule no cjatade");
		ricerca.setUid("");
		ricerca.setTipo(0);

		if(_ricerche == null)
			_ricerche = new ArrayList<Ricerca>();

		_ricerche.add(ricerca);
		_ricercaAdapter.add(ricerca);
	}

    private void parseSearch_FUR(String jString) throws Exception {

    	JSONObject jObject = new JSONObject(jString);

		if(hasError(jObject)){
			Toast.makeText(this, "Riprovare tra qualche momento...", Toast.LENGTH_LONG).show();
			return;
		}

    	// Paging

    	setupPaging(jObject);
		setupSuggestion(jObject);

    	// ----- Paging

		JSONArray vocabolarioArray = jObject.getJSONArray("results");

		for (int i = 0; i < vocabolarioArray.length(); i++) {

			JSONArray ja_ricerca = vocabolarioArray.getJSONArray(i);

			String parola = ja_ricerca.getString(0);

			JSONArray ja_lemmi = null;

			if(_currentLanguage == Ricerca.ITA)
				ja_lemmi = ja_ricerca.getJSONArray(1);
			else if (_currentLanguage == Ricerca.FUR)
				ja_lemmi = ja_ricerca.getJSONArray(2);

			Ricerca ricerca = new Ricerca();

			for(int k=0;k<ja_lemmi.length();k++)
			{
				JSONObject jo_lemma = ja_lemmi.getJSONObject(k);
				String lemma = jo_lemma.getString("lemma");
				String uid = jo_lemma.getString("uid");

				ricerca.getLemmi().add(new KeyValueObject(uid, lemma));
			}

			ricerca.setText(parola);
			ricerca.setTipo(-1);
			ricerca.setLanguage(_currentLanguage);

			if(_ricerche == null)
				 _ricerche = new ArrayList<Ricerca>();

			_ricercaAdapter.add(ricerca);
			_ricerche.add(ricerca);

		}

		addPaging();

    }

    public void parseSearch_ITA(String jString) throws Exception {

    	JSONObject jObject = new JSONObject(jString);

		if(hasError(jObject)){
			Toast.makeText(this, "Riprovare tra qualche momento...", Toast.LENGTH_LONG).show();
			return;
		}

		setupPaging(jObject);
		setupSuggestion(jObject);

		JSONArray vocabolarioArray = jObject.getJSONArray("results");

		String htmlOutput = "";

		for (int i = 0; i < vocabolarioArray.length(); i++) {

			htmlOutput = vocabolarioArray.getJSONObject(i).getString("lemma").toString();
			String uid = vocabolarioArray.getJSONObject(i).getString("uid").toString();
			int type = vocabolarioArray.getJSONObject(i).getInt("type");

			Ricerca ricerca = new Ricerca();

			ricerca.setText(htmlOutput);
			ricerca.setUid(uid);
			ricerca.setTipo(type);
			ricerca.setLanguage(_currentLanguage);

			if(_ricerche == null)
				 _ricerche = new ArrayList<Ricerca>();

			_ricerche.add(ricerca);
			_ricercaAdapter.add(ricerca);

		}

		addPaging();
	}

	private void addPaging(){

		if(_paginaCorrente < _pagineTotali)
		{
			_ricerche.add(new Ricerca(true));
			_ricercaAdapter.add(new Ricerca(true));
		}
	}

	private void setupPaging(JSONObject jObject) throws JSONException
	{
		JSONObject jo = jObject.getJSONObject("paging");

		_paginaCorrente = jo.getInt("current_page");
		_pagineTotali = jo.getInt("total_pages");
	}

	private void RefreshRicercaListView()
	{
		if(_pagineTotali > 1)
		{
			int adjust = 2;

			if(_paginaCorrente == _pagineTotali)
				adjust = 1;

			_ricerche.get(_ricerche.size()-adjust).setPage( String.format("(Pag.: %s di %s)",String.valueOf(_paginaCorrente),String.valueOf(_pagineTotali)));
			_ricercaAdapter.getItem(_ricercaAdapter.getCount()-adjust).setPage(String.format("(Pag.: %s di %s)",String.valueOf(_paginaCorrente),String.valueOf(_pagineTotali)));
		}
		else
			_ricerca_lv.smoothScrollToPosition(0);

		_ricerca_lv.invalidate();

		if(_ricercaAdapter.getCount() == 0)
		{
			Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Nessun risultato", Toast.LENGTH_LONG);
			toast.show();
			setupNoData();
		}

	}

    private class OnCustomItemClickListener implements OnItemClickListener {

		Object _item;
		boolean _esito;

		public OnCustomItemClickListener() {

		}

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
				long arg3) {

				_ricercaSelected  = _ricercaAdapter.getItem(pos);

				if(_ricercaSelected.getPaging())
						incrementaPagina();
				else if(_currentLanguage == Ricerca.ITA)
						InviaRichiestaLemma();
				else if(_currentLanguage == Ricerca.FUR)
						ShowLemmiActivity();
			}

	}

	public boolean onTouch(View arg0, MotionEvent arg1) {
		return false;
	}

	@Override
	public void onBackPressed() {

		if(_currentForm != FORM_RICERCA)
		{
			_currentForm = FORM_RICERCA;
		}
		else //if(!_canExit)
		{
			super.onBackPressed();
		}
//		else
//		{
//			AlertDialog.Builder builder = new AlertDialog.Builder(this);
//			builder.setMessage("Sierâ la aplicazion?")
//					.setTitle(" ")
//					.setIcon(R.drawable.warning)
//					.setCancelable(false)
//					.setPositiveButton("SI",
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog,
//										int id) {
//									RicercaActivity.this.finish();
//								}
//							})
//					.setNegativeButton("NO",
//							new DialogInterface.OnClickListener() {
//								public void onClick(DialogInterface dialog,
//										int id) {
//									dialog.cancel();
//								}
//							});
//			AlertDialog alert = builder.create();
//
//			alert.show();
//		}

	}

    @Override
	public boolean onCreateOptionsMenu(Menu menu) {

    	MenuInflater inflater = getMenuInflater();

    	if(android.os.Build.VERSION.SDK_INT < 11)
    		inflater.inflate(R.menu.menu, menu);
    	else
    		inflater.inflate(R.menu.actionbar_menu, menu);

		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
			case R.id.menu_info:

				info();

				return true;

//			case R.id.menu_cache:
//
//				deleteCache();

			default:
				return super.onOptionsItemSelected(item);
		}

	}

	private void deleteCache(){

		String message = "La aplicazion e ten memorie di dutis lis ricercjis fatis par podêlis viodi ancje se no si è conetûts.\n"
				+ "Scancelâ la memorie di dutis lis ricercjis?";

//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
//		builder.setMessage(message)
//				.setTitle("Atenzion")
//				.setIcon(R.drawable.warning)
//				.setCancelable(false)
//
//				.setPositiveButton("Sì",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//												int id) {
//
//								ApplicationHelper.clearSharedPreferences(false);
//
//								Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Ricerche rimosse correttamente", Toast.LENGTH_LONG);
//								toast.show();
//							}
//						})
//				.setNegativeButton("No",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//												int id) {
//								dialog.cancel();
//							}
//						});

		_cancellaDialog = new Dialog(RicercaActivity.this);
		_cancellaDialog.setContentView(R.layout.dialog_cancella);
		_cancellaDialog.show();
	}

	public void cancellaSI(View v){

		_cancellaDialog.dismiss();

		ApplicationHelper.clearSharedPreferences(false);

		Toast toast = Toast.makeText(ApplicationHelper.CURRENTCONTEXT, "Ricerche rimosse correttamente", Toast.LENGTH_LONG);
		toast.show();
	}

	public void cancellaNO(View v){

		_cancellaDialog.dismiss();
	}

	public void info()
	{
		Intent i = new Intent(this, Info.class);
		startActivity(i);
	}

	private void incrementaPagina()
	{
		if(_paginaCorrente+1 <= _pagineTotali)
			_paginaCorrente++;

		InviaRichiestaRicerca(String.valueOf(_paginaCorrente));
	}

	private void decrementaPagina()
	{
		if(_paginaCorrente-1 >= 1)
			_paginaCorrente--;

		InviaRichiestaRicerca(String.valueOf(_paginaCorrente));
	}
}



	
	

