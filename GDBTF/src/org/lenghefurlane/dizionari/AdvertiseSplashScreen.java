package org.lenghefurlane.dizionari;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.lenghefurlane.dizionari.helper.ApplicationHelper;
import org.lenghefurlane.dizionari.setup.ApplicationSetup;

/**
 * Created by ale on 30/11/16.
 */

public class AdvertiseSplashScreen extends Activity
{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.form_advsplashscreen);

        Button bnContinua = (Button) findViewById(R.id.bnContinua);
        bnContinua.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                NextActivity();
            }
        });

        ImageView ivAdvertise = (ImageView) findViewById(R.id.ivAdvertise);
        ivAdvertise.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                String url = ApplicationSetup.ADVURL;

                if(!TextUtils.isEmpty(url))
                {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }

                return false;
            }
        });

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.listener(new Picasso.Listener()
        {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
            {
                exception.printStackTrace();
            }
        });

        builder.build().load(ApplicationSetup.ADVIMG)
                .placeholder(R.drawable.splash)
                .error(R.drawable.splash)
                .into(ivAdvertise);

    }

    private void NextActivity()
    {
        finish();
        Intent intent=new Intent(this , org.lenghefurlane.dizionari.RicercaActivity.class);
        startActivity(intent);
    }

}


