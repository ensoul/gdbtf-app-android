package org.lenghefurlane.dizionari.setup;

import android.os.Build;

public class ApplicationSetup {

	public static String ADVIMG = "https://www.arlef.it/app/splash.jpg";
	public static String ADVURL = "";

	public static String LEMMA = "get_lemma?uid=";

	public static String getEngineUrl(){
		if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1){
			return "http://arlef.it/cgi-bin/gdbtf_api.pl/";
		}

		return "https://arlef.it/cgi-bin/gdbtf_api.pl/";
	}

	public static boolean DEBUG = false;
}
