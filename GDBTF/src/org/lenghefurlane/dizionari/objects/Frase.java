package org.lenghefurlane.dizionari.objects;

public class Frase {

	String _frase_ita = "";
	String _frase_fur = "";

	public String getFraseIta()
	{
		return _frase_ita;
	}
	
	public String getFraseFur()
	{
		return _frase_fur;
	}
	
	public void setFraseIta(String value)
	{
		_frase_ita = value;
	}
	
	public void setFraseFur(String value)
	{
		_frase_fur = value;
	}
	
}
