package org.lenghefurlane.dizionari.objects;

import java.util.ArrayList;
import java.util.List;

public class Definizione {

	private boolean _roman = false;
	private int _romanIndex = -1;
	private String _title = "";
	private String _cg = "";
	private String _definizione = "";
	private String _text1 = "";
	private String _text2 = "";
	private String _uid;
	private String _degree = "";
	private String _subdegree = "";
	private List<Frase> _frasi;
	private List<Definizione> _sottoDefinizioneList = null;
	
	public Definizione()
	{
		_frasi = new ArrayList<Frase>();
	}

	public int getRomanIndex()
	{
		return _romanIndex;
	}
	public void setRomanIndex(int value){ _romanIndex = value; }

	public boolean isRoman(){ return _romanIndex > -1; }

	public List<Definizione> getSottoDefinizioneList()
	{
		if(_sottoDefinizioneList == null)
			_sottoDefinizioneList = new ArrayList<Definizione>();
		
		return _sottoDefinizioneList;
	}

	public String getDegree()
	{
		return _degree;
	}
	public void setDegree(String value)
	{
		_degree = value;
	}

	public String getSubDegree()
	{
		return _subdegree;
	}
	public void setSubDegree(String value)
	{
		_subdegree = value;
	}

	public String getDefinizione()
	{
		return _definizione;
	}
	public void setDefinizione(String value)
	{
		_definizione = value;
	}
	
	public String getText1()
	{
		return _text1;
	}
	
	public void setText1(String value){	_text1 = value;	}
	public void appendText1(String value){	_text1 += value;	}
	
	public String getText2()
	{
		return _text2;
	}
	
	public void setText2(String value)
	{
		_text2 = value;
	}
	public void appendText2(String value)
	{
		_text2 += value;
	}

	public void setUid(String value)
	{
		_uid = value;
	}
	
	public String getUid()
	{
		return _uid;
	}
	
	public void setTitle(String value)
	{
		_title = value;
	}
	
	public String getTitle()
	{
		return _title;
	}
	
	public void setCG(String value)
	{
		_cg = value;
	}
	
	public String getCG()
	{
		return _cg;
	}
	
	public List<Frase> getFrasi()
	{
		return _frasi;
	}

}

