package org.lenghefurlane.dizionari.objects;

public class MessageObject {

	private String _error;
	private String _output;
	private int _tiporichiesta;
	
	public MessageObject(String output, int tiporichiesta)
	{
		_output = output;
		_tiporichiesta = tiporichiesta;
	}

	public MessageObject(String error, String output, int tiporichiesta)
	{
		_error = error;
		_output = output;
		_tiporichiesta = tiporichiesta;
	}

	public void setError(String value){ _error = value;}
	public String getError(){ return _error; }

	public void setOutput(String output)
	{
		_output = output;
	}
	public String getOutput()
	{
		return _output;
	}

	public void setTipoRichiesta(int tiporichiesta)
	{
		_tiporichiesta = tiporichiesta;
	}
	public int getTipoRichiesta()
	{
		return _tiporichiesta;
	}

}
