package org.lenghefurlane.dizionari.objects;

import java.util.ArrayList;
import java.util.List;

public class Ricerca {

	public static int LEMMA_MONOREMATICO = 1;
	public static int LEMMA_POLIREMATICO = 2;
	public static int CONFISSO = 4;

	public static int ITA = 1;
	public static int FUR = 2;
	
	private int _language = -1;
	
	private List<KeyValueObject> _lemmi;
	private String _text;
	private String _uid;
	private int _type;
	private boolean _paging = false;
	
	private String _page = "";
	
	public Ricerca(boolean paging)
	{
		_paging = paging;
		_lemmi = new ArrayList<KeyValueObject>();
	}
	
	public Ricerca() {
		
		_lemmi = new ArrayList<KeyValueObject>();
	}
	
	public String getText()
	{
		return _text;
	}
	
	public void setText(String value)
	{
		_text = value;
	}
	
	public void setUid(String value)
	{
		_uid = value;
	}
	
	public String getUid()
	{
		return _uid;
	}
	
	public int getTipo()
	{
		return _type;
	}
	
	public void setTipo(int type)
	{
		_type = type;
	}
	
	public void setPaging(boolean paging)
	{
		_paging = paging;
	}
	
	public boolean getPaging()
	{
		return _paging;
	}
	
	public List<KeyValueObject> getLemmi()
	{
		return _lemmi;
	}
	
	public int getLanguage()
	{
		return _language;
	}
	
	public void setLanguage(int value)
	{
		_language = value;
	}
	
	public void setPage(String value)
	{
		_page = value;
	}
	
	public String getPage()
	{
		return _page;
	}
	
}
