package org.lenghefurlane.dizionari.objects;

public class KeyValueObject {

	private String _key;
	private String _value;
	
	public KeyValueObject(String key, String value)
	{
		_value = value;
		_key = key;
	}
	
	public void setKey(String value)
	{
		_key = value;
	}
	
	public String getKey()
	{
		return _key;
	}
	
	public void setValue(String value)
	{
		_value = value;
	}
	
	public String getValue()
	{
		return _value;
	}
	
	public String toString()
	{
	    return( _value );
	}
}
