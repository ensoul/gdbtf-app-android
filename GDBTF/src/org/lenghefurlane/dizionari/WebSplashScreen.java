package org.lenghefurlane.dizionari;

import org.lenghefurlane.dizionari.controls.CustomWebView;
import org.lenghefurlane.dizionari.helper.ApplicationHelper;
import org.lenghefurlane.dizionari.helper.ConnectivityHelper;
import org.lenghefurlane.dizionari.setup.ApplicationSetup;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebSplashScreen extends Activity {

	protected boolean _active = true;
	protected int _splashTime = 2000; 
	
	private CustomWebView mWebView;
	private Thread _splashTread = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    setContentView(R.layout.form_websplashscreen);
	 
	    ApplicationHelper.CURRENTCONTEXT = this.getApplicationContext();
	    
	    if(_splashTread == null)
	    {
	    
		    _splashTread = new Thread() {
		        @Override
		        public void run() {
		            try {
		            	
		            	if(!ApplicationSetup.DEBUG)
		            	{
		            		
			                int waited = 0;
			                while(_active && (waited < _splashTime)) {
			                    sleep(100);
			                    if(_active) {
			                        waited += 100;
			                    }
			                }
			                
		            	}
		                
		            } catch(InterruptedException e) {
		            
		            } finally {
		                
		                NextActivity();
		            }
		        }
		    };
	    
	    }
	    
	    mWebView = (CustomWebView) findViewById(R.id.websplashscreen_webview);
		mWebView.getSettings().setJavaScriptEnabled(false);
		mWebView.getSettings().setDomStorageEnabled(true);
		mWebView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.setOnTouchListener(mWebView.mOnTouchListener);
                return false;
            }
        });
		
		if(ConnectivityHelper.isOnline() && !ApplicationSetup.DEBUG)
			mWebView.loadUrl(ApplicationSetup.ADVURL);
		else
			NextActivity();
		
		mWebView.setWebViewClient(new WebViewClient() {

			   public void onPageFinished(WebView view, String url) {
			        
				  if(!_splashTread.isAlive())
					   _splashTread.start();
//				  else
//					  NextActivity();
			    }
		});
	  
	    
	    
	}
	
	private void NextActivity()
	{
		finish();
        Intent intent=new Intent(this , org.lenghefurlane.dizionari.RicercaActivity.class);
        startActivity(intent);
	}
	
	@Override
	public void onBackPressed() {
		
	}
	
}
