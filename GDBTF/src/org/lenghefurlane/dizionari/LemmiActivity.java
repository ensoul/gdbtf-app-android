package org.lenghefurlane.dizionari;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import org.lenghefurlane.dizionari.adapters.LemmaAdapter;
import org.lenghefurlane.dizionari.helper.ApplicationHelper;
import org.lenghefurlane.dizionari.objects.KeyValueObject;
import org.lenghefurlane.dizionari.objects.Ricerca;

/**
 * Created by ale on 29/08/16.
 */
public class LemmiActivity extends GDBTFActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.form_lemmi);

        ListView lemmi_lv = (ListView) findViewById(R.id.popup_lemmi_lv);

        List<KeyValueObject> data = new ArrayList<KeyValueObject>();
        for(KeyValueObject lemma: ApplicationHelper.RICERCA_SELECTED.getLemmi())
        {
            data.add(new KeyValueObject(lemma.getKey(), lemma.getValue()));
        }

        final LemmaAdapter lemmiAdapter = new LemmaAdapter(LemmiActivity.this,R.layout.lemma_item, data);
        lemmi_lv.setAdapter(lemmiAdapter);

        lemmi_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {

                KeyValueObject lemma = lemmiAdapter.getItem(pos);

                _ricercaSelected = new Ricerca();
                _ricercaSelected.setTipo(0);
                _ricercaSelected.setUid(lemma.getKey());

                InviaRichiestaLemma();
            }
        });
    }

}
